## Proyecto de ejemplo de Integración Continua en Gitlab

Se utiliza un proyecto desarrollado en Python - Django.

En el archivo .gitlab-ci.yml se definen tres ambientes de configuración:
 - Desarrollo - Pruebas Unitarias (tests): se ejecutan las pruebas unitarias del sistema utilizando Gitlab Runner.
 - Homologación (staging): Se despliega el proyecto (la rama master) a Heroku en [https://proyecto-staging.herokuapp.com](https://proyecto-staging.herokuapp.com).
 - Producción (production): Se despliega el proyecto (las versiones marcadas con tags) a Heroku en [https://proyecto-production.herokuapp.com](https://proyecto-production.herokuapp.com).
 