# -*- coding: utf-8 -*-
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.shortcuts import redirect, render
from django.urls import reverse
from django.views.generic import TemplateView


class IndexView(LoginRequiredMixin, TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['breadcrumb'] = [{'nombre': 'Inicio', 'url': '#'}]
        return context


@login_required(login_url='/login/')
def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Importante para la sesión
            messages.success(request, 'Tu contraseña fue actualizada correctamente!')
            return HttpResponseRedirect(reverse('index'))
    else:
        form = PasswordChangeForm(request.user)

    return render(request, 'password/change_password.html', {
        'form': form,
        'titulo': 'Cambiar contraseña'
    })
